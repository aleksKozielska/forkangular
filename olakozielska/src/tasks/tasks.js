
const tasks = angular.module("tasks", []);

tasks.controller('TasksCtrl', ($scope) =>{
    $scope.tasks = [{
        id: "123",
        name: "Task 123",
        completed: true
      },{
        id: "234",
        name: "Task 234",
        completed: false
      },{
        id: "345",
        name: "Task 345",
        completed: true
      }];

      $scope.taskSelected = {
        id: "123",
        name: "Task 123",
        completed: true
      }
    $scope.edit = ()=> {
        $scope.taskCopied = {...$scope.task}
        $scope.task.isEdited = true;
    }
    $scope.cancel = ()=> {
        $scope.taskCopied = {...$scope.task}
        $scope.task.isEdited = false;
    }
    $scope.save = ()=> {
        console.log($scope.taskCopied);
        $scope.task = {...$scope.taskCopied}
    }
    $scope.selectTask = ()=> {
        $scope.taskSelected = task;
    }
})
