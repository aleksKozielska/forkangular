
const app = angular.module("myApp", ["config", "tasks"]);
angular.module("config", []);

app.run(function ($rootScope){})

app.controller('AppCtrl', ($scope) =>{
    $scope.title = "MyApp";
    $scope.user = { name: "Guest"};
    $scope.isLoggedIn = false;
    $scope.logIn = () => {
        $scope.user.name = "Admin";
        $scope.isLoggedIn = true;
    }
    $scope.logOut = () => {
        $scope.user.name = "Guest";
        $scope.isLoggedIn = false;
    }
})

angular.bootstrap(document, ['myApp'])