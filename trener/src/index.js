
angular.module('config', [])

const app = angular.module('myApp', [
  'config', 'tasks'
])

// Global scope
// app.run(function ($rootScope) { })

app.controller('AppCtrl', ($scope) => {
  $scope.title = 'MyApp'
  $scope.user = { name: 'Guest' }

  $scope.isLoggedIn = false;

  $scope.login = () => {
    $scope.user.name = 'Admin'
    $scope.isLoggedIn = true
  }

  $scope.logout = () => {
    $scope.user.name = 'Guest'
    $scope.isLoggedIn = false
  }
})


// Start !

// <html ng-app="myApp" // auto-boostrap
angular.bootstrap(document, ['myApp']) // manual boostrap