const app = angular.module('app', ['InvoiceEditor'])

const invoiceEditor = angular.module('InvoiceEditor', [])
invoiceEditor.controller('InvoiceEditorCtrl', ($scope) => {

  const invoice = {
    positions: [
      {
        id: "123",
        title: 'Pozycja 123',
        netto: 1000,
        tax: 23,
        brutto: 1230
      }
    ], 
    summary : {
      total: 1230
    }
  }
  $scope.invoice = invoice

  $scope.addPosition = () => { 
    $scope.invoice.positions.push(
      {
        id: "",
        title: '',
        netto: 0,
        tax: 0,
        brutto: 0
      }
    )
  }

  $scope.removePosition = (position) => { 
    console.log(position);
    var index = $scope.invoice.positions.indexOf(position);
    $scope.invoice.positions.splice(index, 1);
  }

  $scope.countAllInvoices = () => {
    var sum = $scope.invoice.positions.reduce(function(previousValue, currentValue) {
      return previousValue + currentValue.brutto;
    }, 0);
    $scope.invoice.summary.total = sum;
  }
})

angular.bootstrap(document, ['app'])
