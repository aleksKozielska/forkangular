
const app = angular.module('app', ['InvoiceEditor'])

const invoiceEditor = angular.module('InvoiceEditor', [])
invoiceEditor.controller('InvoiceEditorCtrl', ($scope) => {

  const invoice = {
    positions: [
      {
        id: "123",
        title: 'Pozycja 123',
        netto: 1000,
        tax: 23,
        brutto: 1230
      }
    ], summary = {
      total: 1230
    }
  }
  $scope.invoice = invoice

  $scope.addPosition = () => { }
  $scope.removePosition = () => { }

})
